import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nguyen Thanh An", "annt@gmail.com", 'm');
        System.out.println("Author1:");
        System.out.println(author1);

        Author author2 = new Author("Le Viet Bac", "bacle@gmail.com", 'f');
        System.out.println("Author2:");
        System.out.println(author2.toString());

        Book book1 = new Book("Java", author1, 30000);
        System.out.println("Book1:");
        System.out.println(book1);

        Book book2 = new Book("ReactJs", author2, 50000, 100);
        System.out.println("Book2:");
        System.out.println(book2);        
    }
}
